from django.conf.urls import url

from orders.views import OrderFormView, thank
from .views import CartCreateView

urlpatterns = [
    url(r'^$', CartCreateView.as_view(), name='create_cart'),
    url(r'^order/$', OrderFormView.as_view(), name='order'),
    url(r'^order/thank/$', thank, name='thank'),
]