from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def current_count(context, product):
    for cart_product, quantity, full_summ in context['products_info']:
        if cart_product == product:
            return quantity
    return 0


@register.simple_tag(takes_context=True)
def current_price(context, product):
    for cart_product, quantity, full_summ in context['products_info']:
        if cart_product == product:
            return int(full_summ)
    return int(product.price)