from .utils import get_cart_params


def cart(request):
    products_info, full_sum, full_weight = get_cart_params(request.session)
    return {'products_info': products_info,
            'full_sum': full_sum,
            'full_weight': full_weight,
            'full_count': sum([q for p, q, s in products_info])}