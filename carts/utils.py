from products.models import Product


def get_cart_params(session, **kwargs):
    item_id = kwargs.get('item', '')
    quantity = kwargs.get('qty', '1')
    if quantity.isdigit() and item_id.isdigit():
        item_id = int(item_id)
        quantity = int(quantity)
        if not session.get('cart'):
            session['cart'] = []

        added = False
        for index, (cart_product_id, cart_quantity) in enumerate(session['cart']):
            if cart_product_id == item_id:
                session['cart'][index] = (cart_product_id, quantity)
                added = True
                break
        if not added:
            session['cart'].append((item_id, quantity))
        session.save()

    product_qs = Product.objects.filter(pk__in=(p for p, q in session.get('cart', {})))
    products_in_cart = {p.pk: p for p in product_qs}

    products_info = [(products_in_cart[i], q, products_in_cart[i].price * q) for i, q in session.get('cart', {})]

    full_sum = sum((pr for *i, pr in products_info))
    full_weight = sum((p.calorie * q for p, q, pr in products_info))

    return products_info, full_sum, full_weight