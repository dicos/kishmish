from django.views.generic.base import TemplateView
from django.http import Http404
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator


class CartCreateView(TemplateView):
    template_name = 'new/_cart_products.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def post(self, request):
        item_id = request.POST.get('item', '')
        quantity = request.POST.get('qty', '1')
        if not quantity.isdigit() or not item_id.isdigit():
            raise Http404
        item_id = int(item_id)
        quantity = int(quantity)
        if not request.session.get('cart'):
            request.session['cart'] = []

        added = False
        for index, (cart_product_id, cart_quantity) in enumerate(request.session['cart']):
            if cart_product_id == item_id:
                if not quantity:
                    del request.session['cart'][index]
                else:
                    request.session['cart'][index] = (cart_product_id, quantity)
                added = True
                break
        if not added and quantity:
            request.session['cart'].append((item_id, quantity))
        request.session.save()

        return super().get(request)
