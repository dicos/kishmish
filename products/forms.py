from django import forms

from .models import Product


class ProductForm(forms.ModelForm):
	class Meta:
		model = Product
		fields = ('title', 'price', 'sale_price', 'active')


ProductFormSet = forms.models.modelformset_factory(Product, form=ProductForm, extra=1)