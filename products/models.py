from django.db import models
from django.db.models import Q
from django.urls import reverse


class Category(models.Model):
    title = models.CharField(max_length=100)
    active = models.BooleanField(default=True)
    image = models.ImageField(u'Фото категории', upload_to='category/')
    subimage = models.ImageField(u'Картинка под фото', upload_to='category/')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('category_detail', kwargs={'slug': self.slug})

    class Meta:
        verbose_name = 'Категория продуктов'
        verbose_name_plural = 'Категории продуктов'


class Product(models.Model):
    title = models.CharField('Название', max_length=100)
    slug = models.CharField(max_length=100, unique=True)
    price = models.DecimalField('Цена', max_digits=7, decimal_places=2)
    sale_price = models.DecimalField('Цена со скидкой', max_digits=7, decimal_places=2, blank=True, null=True)
    composition = models.TextField('Состав')
    weight = models.IntegerField('Вес, гр.')
    calorie = models.IntegerField('Калорийность, ккал')
    protein = models.IntegerField('Белков')
    carbohydrate = models.IntegerField('Углеводов')
    fat = models.IntegerField('Жиров')
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    description = models.TextField('Традиция блюда')
    image = models.ImageField('Фото', upload_to='products/', null=True)
    image_small = models.ImageField('Маленькое фото', upload_to='products/', null=True)
    show_main = models.BooleanField('На главную страницу', default=False)
    recomendations = models.ManyToManyField('self', verbose_name='Рекомендуемые продукты', blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('product_detail', args=(self.slug,))

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'
