from django.contrib import admin

from .models import Product, Category


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'price')
    prepopulated_fields = {"slug": ("title",)}
    filter_horizontal = ('recomendations',)


admin.site.register(Category)