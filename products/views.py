from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.db.models import Q

from .models import Product


class ProductListView(ListView):
    """
    """
    model = Product
    template_name = 'new/menu.html'
    
    def get_queryset(self, *args, **kwargs):
        queryset = super(ProductListView, self).get_queryset(*args, **kwargs)
        query = self.request.GET.get('q')
        if query:
            queryset = self.model.objects.filter(
                        Q(title__icontains=query)|
                        Q(description__icontains=query)
                        )
        return queryset.prefetch_related('recomendations')

    def get_context_data(self, *args, **kwargs):
        context = super(ProductListView, self).get_context_data(*args, **kwargs)
        return context


class ProductDetailView(DetailView):
    """
    class based product detail view
    """
    model = Product
    slug_url_kwarg = 'slug'
    template_name = 'new/dish.html'

    def get_context_data(self, **kwargs):
        context = super(ProductDetailView, self).get_context_data(**kwargs)
        obj = self.get_object()
        context['related_products'] = obj.recomendations.filter(active=True)
        return context
