# Generated by Django 2.1.7 on 2019-03-23 12:54

import datetime
from django.db import migrations, models
import django.db.models.deletion
import enumfields.fields
import orders.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('delivery', enumfields.fields.EnumField(enum=orders.models.Order.Delivery, max_length=10, verbose_name='Способ самовывоза')),
                ('name', models.CharField(max_length=100, verbose_name='Имя')),
                ('phone', models.CharField(max_length=30, verbose_name='Телефон')),
                ('payment', enumfields.fields.EnumField(enum=orders.models.Order.Payment, max_length=10, verbose_name='Способы оплаты')),
                ('money', models.PositiveSmallIntegerField(default=0, verbose_name='Сдача с суммы')),
                ('count', models.PositiveSmallIntegerField(verbose_name='Укажите количество человек')),
                ('comment', models.TextField(verbose_name='Комментарий')),
                ('full_price', models.DecimalField(decimal_places=2, max_digits=7, verbose_name='Цена за заказ')),
                ('full_weight', models.IntegerField(verbose_name='Вес всего заказа, гр.')),
                ('created', models.DateTimeField(default=datetime.datetime.now, verbose_name='Дата создания')),
                ('checked', models.BooleanField(default=False, verbose_name='Обработан?')),
            ],
            options={
                'verbose_name': 'Заказ',
                'verbose_name_plural': 'Заказы',
            },
        ),
        migrations.CreateModel(
            name='OrderPorduct',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100, verbose_name='Название')),
                ('price', models.DecimalField(decimal_places=2, max_digits=7, verbose_name='Цена')),
                ('full_price', models.DecimalField(decimal_places=2, max_digits=7, verbose_name='Цена за все продукты')),
                ('weight', models.IntegerField(verbose_name='Вес, гр.')),
                ('full_weight', models.IntegerField(verbose_name='Вес продуктов, гр.')),
                ('quantity', models.PositiveSmallIntegerField(verbose_name='Количество')),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='orders.Order', verbose_name='Заказ')),
                ('product', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='products.Product', verbose_name='Продукт')),
            ],
            options={
                'verbose_name': 'Продукты в заказе',
                'verbose_name_plural': 'Продукты в заказах',
            },
        ),
        migrations.CreateModel(
            name='PickupPoint',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='Точка выдачи')),
            ],
            options={
                'verbose_name': 'Точка выдачи',
                'verbose_name_plural': 'Точки выдачи',
            },
        ),
        migrations.AddField(
            model_name='order',
            name='pikup_point',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='orders.PickupPoint', verbose_name='Точка выдачи'),
        ),
    ]
