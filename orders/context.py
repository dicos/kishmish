from .forms import OrderForm
from .models import Order


def form(request):
    return {'form': OrderForm(request.POST or None),
            'Payment': Order.Payment.__members__,
            'Delivery': Order.Delivery.__members__}