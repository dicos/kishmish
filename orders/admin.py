from django.contrib import admin

from .models import Order, OrderPorduct, PickupPoint


class OrderProductInline(admin.StackedInline):
    model = OrderPorduct


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_filter = ('checked',)
    list_display = list_display_links = ('name', 'phone', 'full_price', 'created', 'checked')
    inlines = [OrderProductInline]


admin.site.register(PickupPoint)