from django.conf import settings
from django.contrib import messages
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.shortcuts import render, redirect
from django.utils.html import strip_tags
from django.views.generic.edit import FormView
from django.urls import reverse

from carts.utils import get_cart_params
from newsletter.models import Config
from .forms import OrderForm
from .models import OrderPorduct


class OrderFormView(FormView):
    form_class = OrderForm
    http_method_names = [ 'post', 'put']

    def get(self, request, *args, **kwargs):
        """Handle GET requests: instantiate a blank version of the form."""
        return self.render_to_response(self.get_context_data())

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if not request.session.get('cart'):
            return self.form_invalid(form)
        return super().post(request, *args, **kwargs)

    def form_valid(self, form, *args, **kwargs):
        order = form.save(commit=False)
        products_info, order.full_price, order.full_weight = get_cart_params(self.request.session)
        order.save()

        order_products = []
        for product, quantity, full_price in products_info:
            order_products.append(OrderPorduct(order=order,
                                               product=product,
                                               title=product.title,
                                               price=product.price,
                                               full_price=full_price,
                                               weight=product.weight,
                                               full_weight=(product.weight * quantity),
                                               quantity=quantity))
        OrderPorduct.objects.bulk_create(order_products)

        html_content = render_to_string('new/order_email.html', {'order': order})
        text_content = strip_tags(html_content)
        config = Config.get_solo()
        msg = EmailMultiAlternatives('Заказ с сайта', text_content, settings.EMAIL_HOST_USER, [config.contact_email])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        messages.info(self.request, 'Заказ оформлен')
        return super().form_valid(form, *args, **kwargs)

    def form_invalid(self, form):
        print(form.errors)
        messages.error(self.request, form.errors)
        return redirect(self.request.POST.get('url') or '.')

    def get_success_url(self):
        del self.request.session['cart']
        return reverse('thank')


def thank(request):
    return render(request, 'new/thank.html')