from datetime import datetime

from django.db import models

from enumfields import EnumField, Enum

from products.models import Product


class PickupPoint(models.Model):
    name = models.CharField('Точка выдачи', max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Точка выдачи'
        verbose_name_plural = 'Точки выдачи'


class Order(models.Model):
    class Delivery(Enum):
            SELF = 1
            COURIER = 2
            class Labels:
                SELF = 'Самовывоз'
                COURIER = 'Курьерская доставка'

    class Payment(Enum):
        CASH = 1
        CARD = 2
        class Labels:
            CASH = 'Наличными'
            CARD = 'Картой курьеру'

    delivery = EnumField(Delivery, verbose_name='Способ самовывоза')
    pikup_point = models.ForeignKey(PickupPoint, verbose_name='Точка выдачи', blank=True, null=True,
                                    on_delete=models.SET_NULL)
    name = models.CharField('Имя', max_length=100)
    street = models.CharField('Улица', max_length=255, blank=True, null=True)
    house = models.CharField('Дом', max_length=50, blank=True, null=True)
    apartment = models.CharField('Номер', max_length=50, blank=True, null=True)
    phone = models.CharField('Телефон', max_length=30)
    payment = EnumField(Payment, verbose_name = 'Способы оплаты')
    money = models.PositiveSmallIntegerField('Сдача с суммы', default=0, blank=True, null=True)
    count = models.PositiveSmallIntegerField('Укажите количество человек', default=1)
    comment = models.TextField('Комментарий', blank=True)
    full_price = models.DecimalField('Цена за заказ', max_digits=7, decimal_places=2)
    full_weight = models.IntegerField('Вес всего заказа, гр.')
    created = models.DateTimeField('Дата создания', default=datetime.now)
    checked = models.BooleanField('Обработан?', default=False)

    def __str__(self):
        return 'Заказ на %s от %s' % (self.name, self.created)

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'


class OrderPorduct(models.Model):
    order = models.ForeignKey(Order, verbose_name='Заказ', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, verbose_name='Продукт', on_delete=models.SET_NULL, blank=True, null=True)
    title = models.CharField('Название', max_length=100)
    price = models.DecimalField('Цена', max_digits=7, decimal_places=2)
    full_price = models.DecimalField('Цена за все продукты', max_digits=7, decimal_places=2)
    weight = models.IntegerField('Вес, гр.')
    full_weight = models.IntegerField('Вес продуктов, гр.')
    quantity = models.PositiveSmallIntegerField('Количество')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Продукты в заказе'
        verbose_name_plural = 'Продукты в заказах'