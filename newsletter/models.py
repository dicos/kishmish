from datetime import date, timedelta

from django.db import models

from solo.models import SingletonModel


class Config(SingletonModel):
    copyright = models.TextField('Копирайт', default='2019 г. Все права защищены.<br>Политика конфедециальности')
    # контакты
    email = models.EmailField(blank=True)
    vk = models.URLField('Вконтакте', default='https://vk.com/')
    fb = models.URLField('Фейсбук', default='https://fb.com/')
    instagramm = models.URLField('Инстаграмм', default='https://www.instagram.com/')
    text_contacts = models.TextField('Если отправили сообщение со страницы "Контакты"',
                                     default='Ваше сообщение было отправлено. В ближайшее время с вами свяжемся')
    contact_email = models.EmailField('Email', default='qwe@qwe.qwe')
    map_in_contact = models.TextField('Код карты для страницы "контакты"', blank=True)
    phone_formatted = models.TextField('Телефон поддержки форматированный', default='8 800 123 12 12')
    phone_raw = models.TextField('Телефон только цифры', default='88001231212')
    work_time = models.TextField('Время работы', default='с 9.00 до 21.00')
    work_phone = models.TextField('Часы работы телефона поддержки', default='Будние дни с 11:00 до 23:00')
    work_support_weekday = models.TextField('Время работы поддержки в будние', default='Пн.-Пн. с 9.00 до 21.00')
    work_support_weekend = models.TextField('Время работы поддержки в выходные', default='Сб.-Вс. с 12.00 до 18.00')
    #Доставка
    price = models.TextField('Стоимость доставки',
                             default='Какое-то описание стоимости доставки',
                             help_text='Страница "Доставка"')
    packaging = models.TextField('Упаковка доставки',
                                 default='Какое-то описание упаковки',
                                 help_text='Страница "Доставка"')
    speed = models.TextField('Скорость доставки',
                             default='Какое-то описание скорости доставки',
                             help_text='Страница "Доставка"')
    #О нас
    about_header1 = models.TextField('Текст на картинке',
                                     default='ЛЮБОВЬ - САМАЯ СИЛЬНАЯ ИЗ ВСЕХ СТРАСТЕЙ, ПОТОМУ ЧТО ОНА ОДНОВРЕМЕННО '
                                             'ЗАВЛАДЕВАЕТ ГОЛОВОЙ, СЕРДЦЕМ И ЖЕЛУДКОМ.',
                                     help_text='Страница "О нас"')
    about_header2 = models.TextField('Текст на картинке',
                                     default='Традиции позволяют нам насладиться не только душевными историями о '
                                             'Дальнем Востоке, но и траициоными блюдами, которые мы вас хотим '
                                             'накормить!',
                                     help_text='Страница "О нас"')
    about_header3 = models.TextField('Заголовок страницы',
                                     default='НАШИ ТЕХНОЛОГИИ В ПРИГОТОВЛЕНИИ',
                                     help_text='Страница "О нас"')
    about_content = models.TextField('Содержимое страницы',
                                     default='Текст страницы',
                                     help_text='Страница "О нас"')
    # Главная
    index_header1 =  models.TextField('Текст на главной',
                                     default='Идя на рынок - думаем только о качестве: ищим самое свежее мясо, '
                                             'выбираем самые сочные овощи, насыпаем самые ароматные специи. '
                                             'Выбираем разумом, готовим сердцем.',
                                     help_text='Страница "Главная"')
    index_header2 =  models.TextField('Текст на главной',
                                     default='Помни о традициях восточной кухни. Храни рецепты, доставшиеся от предков, '
                                             'но делись своим мастерством с другими. Готовь плов ароматным, как в '
                                             'Узбекистане, а хаш наваристым - как на Кавказе.',
                                     help_text='Страница "Главная"')
    index_header3 =  models.TextField('Текст на главной',
                                     default='Вари, жарь и запекай только на открытом огне. Готовь навристый суп в '
                                             'казане, сочный кебаб на глях, а мягкие лепешки в тандыре.',
                                     help_text='Страница "Главная"')
    index_header4 =  models.TextField('Текст на главной',
                                     default='Оборудование сделанное своими руками',
                                     help_text='Страница "Главная"')
    index_header5 =  models.TextField('Текст на главной',
                                     default='60 лет совместного опыта',
                                     help_text='Страница "Главная"')
    index_header6 =  models.TextField('Текст на главной',
                                     default='Удобная упаковка',
                                     help_text='Страница "Главная"')
    index_header7 =  models.TextField('Текст на главной',
                                     default='Продукты с родины блюд',
                                     help_text='Страница "Главная"')
    index_header8 =  models.TextField('Текст на главной',
                                     default='Порции от 500гр',
                                     help_text='Страница "Главная"')
    index_header9 =  models.TextField('Текст на главной',
                                     default='При покупке от 2 кг скидка',
                                     help_text='Страница "Главная"')
    index_header10 =  models.TextField('Текст на главной',
                                     default='Настоящий тандыр',
                                     help_text='Страница "Главная"')
    index_header11 =  models.TextField('Текст на главной',
                                     default='Фирменные рецепты',
                                     help_text='Страница "Главная"')
    index_header12 =  models.TextField('Текст на главной',
                                     default='Настоящие специи',
                                     help_text='Страница "Главная"')
    index_header13 =  models.TextField('Текст на главной',
                                     default='Предлагаем попробовать популярные блюда',
                                     help_text='Страница "Главная"')
    index_header14 =  models.TextField('Текст на главной',
                                     default='приготовленные нашим шеф-поваром',
                                     help_text='Страница "Главная"')


class DeliveryArea(models.Model):
    name = models.CharField('Название зоные доставки', max_length=50)
    price = models.CharField('Стоимость', default='Бесплатно', max_length=20)
    description = models.TextField('Описание')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Зона доставки'
        verbose_name_plural = 'Зоны доставки'


class Action(models.Model):
    name = models.CharField('Название акции', max_length=100)
    description = models.TextField('Описание')
    content = models.TextField('Условие')
    expired = models.DateField('Действует до', default=date.today)
    image = models.ImageField('Картинка', upload_to='actions/')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Акция'
        verbose_name_plural = 'Акции'


class MainBanner(models.Model):
    banner = models.ImageField('Баннер', upload_to='index-page')
    title = models.CharField('Заголовок', max_length=250)
    text = models.TextField('Описание')

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'Баннер на главную'
        verbose_name_plural = 'Баннеры на главную'


class Instagramm(models.Model):
    photo = models.ImageField('Фото', upload_to='instagramm')
    title = models.CharField('Текст', max_length=250)
    url = models.URLField('Ссылка на инстаграмм', max_length=1000)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Фотография инстаграмма'
        verbose_name_plural = 'Фотографии инстаграмма'
        ordering = ('-pk',)


class Comment(models.Model):
    photo = models.ImageField('Фото', upload_to='comment')
    name = models.CharField('Имя', max_length=250)
    profession = models.CharField('Профессия', max_length=250)
    text = models.TextField('Текст')
    url = models.URLField('Ссылка на текст', max_length=1000)

    def __str__(self):
        return ' '.join((self.name, self.profession, self.text, self.url,))

    class Meta:
        verbose_name = 'Отзыв клиента'
        verbose_name_plural = 'Отзывы клиентов'
        ordering = ('-pk',)