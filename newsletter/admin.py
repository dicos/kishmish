from django.contrib import admin

from solo.admin import SingletonModelAdmin

from .models import Config, DeliveryArea, Action, MainBanner, Instagramm, Comment


@admin.register(Action)
class ActionAdmin(admin.ModelAdmin):
    list_display = list_display_links = ('name', 'expired',)


@admin.register(MainBanner)
class MainBannerAdmin(admin.ModelAdmin):
    list_display = list_display_links = ('title', 'text',)


@admin.register(Instagramm)
class InstagrammAdmin(admin.ModelAdmin):
    list_display = list_display_links = ('title', 'url')


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = list_display_links = ('name', 'profession', 'text', 'url')


admin.site.register(DeliveryArea, admin.ModelAdmin)
admin.site.register(Config, SingletonModelAdmin)