from datetime import date
from django.core.mail import EmailMessage
from django.contrib import messages
from django.shortcuts import render, redirect

from .forms import ContactForm
from products.models import Product
from .models import Config, DeliveryArea, Action, MainBanner, Instagramm, Comment

# Create your views here.
def home(request):

    context = {
        "products": Product.objects.filter(show_main=True),
        'main_banners': MainBanner.objects.all(),
        'instagramms': Instagramm.objects.all(),
        'comments': Comment.objects.all(),
    }

    return render(request, "new/index.html", context)


def contacts(request):
    form = ContactForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form_email = form.cleaned_data.get("email")
        form_message = form.cleaned_data.get("message")
        form_full_name = form.cleaned_data.get("full_name")
        form_phone= form.cleaned_data.get("form_phone")
        config = Config.get_solo()
        contact_message = "От %s\n" \
                          "Тел: %s\n" \
                          "Email: %s\n" \
                          "\n%s"
        contact_message = contact_message % (form_full_name, form_phone, form_email, form_message, )
        mail = EmailMessage('Сообщение с сайта (страница "Контакты")',
                            contact_message, config.email, [form_email])
        if 'document' in form.files:
            mail.attach(form.files['document'], form.files['document'].read(), form.files['document'].content_type)
        mail.send(fail_silently=False)
        messages.add_message(request, messages.INFO, config.text_contacts)
        return redirect('.')

    return render(request, "new/contacts.html")


def delivery(request):
    context = {
        "areas": DeliveryArea.objects.all(),
    }
    return render(request, "new/delivery.html", context)


def actions(request):
    context = {'actions': Action.objects.order_by('-expired')}
    return render(request, 'new/stock.html', context)


def about(request):
	return render(request, 'new/about-us.html', {})