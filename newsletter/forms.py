from django import forms

# from .models import SignUp

class ContactForm(forms.Form):
	full_name = forms.CharField(label='Ваше имя')
	email = forms.EmailField(label='Ваш email')
	phone = forms.CharField(label='Ваш телефон')
	message = forms.CharField(label='Текст сообщения')
	document = forms.FileField(label='Прикрепить документ', required=False)
