from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

from newsletter.views import home, contacts, delivery, actions, about

urlpatterns = [
    # Examples:
    url(r'^$', home, name='home'),
    url(r'^contacts/$', contacts, name='contacts'),
    url(r'^about/$', about, name='about'),
    url(r'^delivery/$', delivery, name='delivery'),
    url(r'^actions/$', actions, name='actions'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^menu/', include('products.urls')),
    url(r'^cart/', include('carts.urls')),

]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)